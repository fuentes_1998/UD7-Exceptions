package com.company;

public class Student {

    private String name;

    private int age;

    private int height;

    Student(String name, int age, int height) {

        this.name = name;
        this.age = age;
        this.height = height;
    }

    public String toString() {

        return this.name + ": " + this.age + " años, " + this.height + " cm.\n";
    }

    public int getAge() {

        return this.age;
    }

    public String getName() {

        return this.name;
    }
}
