package com.company;

public class Main {

    public static void main(String[] args) {

        StudentFactory studentFactory = new StudentFactory();

        Student student1 = studentFactory.create();

        Student student2 = studentFactory.create();

        System.out.println(student1);

        System.out.println(student2);

        if (student1.getAge() > student2.getAge()) {

            System.out.println(student1.getName() + " es mayor que " + student2.getName() + ".");

        } else if(student2.getAge() > student1.getAge()) {

            System.out.println(student2.getName() + " es mayor que " + student1.getName() + ".");

        } else {

            System.out.println(student1.getName() + " y " + student2.getName() + " tienen la misma edad.");
        }
    }
}
