package com.company;

import java.util.InputMismatchException;
import java.util.Scanner;

public class StudentFactory {

    private Scanner sc;

    StudentFactory() {

        this.sc = new Scanner(System.in);
    }

    public Student create() {

        return new Student(getName(), getAge(), getHeight());
    }

    private String getName() {

        System.out.println("Introduce el nombre del alumno:");

        return this.sc.next();
    }

    private int getAge() {

        try{

            System.out.println("Introduce la edad del alumno:");

            return this.sc.nextInt();

        } catch(InputMismatchException e) {

            System.out.println("Debe introducir una edad válida.");

            this.sc.next();

            return getAge();
        }
    }

    private int getHeight() {

        try{

            System.out.println("Introduce la altura (cm) del alumno:");

            return this.sc.nextInt();

        } catch(InputMismatchException e) {

            System.out.println("Debe introducir una altura válida.");

            this.sc.next();

            return getHeight();
        }
    }
}
