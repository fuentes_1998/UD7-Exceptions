package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        int numErrors = 0;

        ArrayList<Float> numberList = new ArrayList<>();

        do {

            try {

                numberList.add(askForNumberWithoutScanner(numberList.size()+1));

            } catch (IOException e) {

                System.out.println("Debe introducir un numero");
                numErrors++;

            } catch (NumberFormatException e) {

                System.out.println("Debe introducir un numero");
                numErrors++;
            }

        }while (numberList.size() < 10);

        System.out.println("El numero maximo introducido es: " + getMax(numberList));
        System.out.println("El numero de errores es: " + numErrors);
    }

    private static float askForNumberWithoutScanner(int position) throws IOException {

        System.out.println("Introduce el numero para la posicion " + position);

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

        return Float.parseFloat(in.readLine());
    }

    private static float getMax(ArrayList<Float> numberList) {

        float maxNumber = 0;

        for (int i = 0; i < numberList.size();i++) {

            if (maxNumber < numberList.get(i)) {

                maxNumber = numberList.get(i);
            }
        }

        return maxNumber;
    }
}
