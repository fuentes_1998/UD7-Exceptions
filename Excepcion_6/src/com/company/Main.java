package com.company;

public class Main {

    public static void main(String[] args) {

        int dividend = 2;

        int[] divisorList = {-2, -1, 0, 1, 2};

        divideByArray(dividend, divisorList);

        System.out.println();

        divideByArrayProvide(dividend, divisorList);
    }

    private static void divideByArray(int dividend, int[] divisorList) {

        for (int i = 0; i < divisorList.length; i++) {

            try{

                System.out.printf("%d / %d = %d",
                        dividend,
                        divisorList[i],
                        dividend / divisorList[i]);

            } catch(ArithmeticException e){

                System.out.print("No se puede dividir entre zero.");

            }

            System.out.println();
        }
    }

    private static void divideByArrayProvide(int dividend, int[] divisorList) {

        for (int i = 0; i < divisorList.length; i++) {

            if (divisorList[i] == 0) {

                System.out.println("No se puede dividir entre zero.");

            } else {

                System.out.printf("%d / %d = %d\n",
                        dividend,
                        divisorList[i],
                        dividend / divisorList[i]);
            }
        }
    }
}
