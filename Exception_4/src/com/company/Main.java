package com.company;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        int[] numbers = new int[5];

        checkArray(numbers);
    }

    private static int addNumber() {

        Scanner sc = new Scanner(System.in);

        return sc.nextInt();
    }

    private static void checkArray(int[] numbers) {

        try {

            makeArray(numbers);

        } catch (NullPointerException e) {

            System.out.println("El array todavía no ha sido creado.");
        }
    }

    private static void makeArray(int[] numbers) {

        int aux = 0;

        int counter = 0;

        do {

            try {

                System.out.println("Introduce un número:");

                numbers[counter] = addNumber();

                counter++;

            } catch (InputMismatchException e) {

                System.out.println("Debe introducir un número");

            } catch (ArrayIndexOutOfBoundsException e1) {

                System.out.println("Ha sobrepasado el tamaño del array");

                aux++;
            }

        } while (counter != 10 && aux == 0);
    }
}
