package com.company;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int number = 0;

        String finalNumber = "";

        do {

            try{

                System.out.println("Introduce un número entre 1 y 5:");

                number = sc.nextInt();

                finalNumber += "" + number;

            } catch(InputMismatchException e) {

                System.out.println("Número inválido");

                sc.next();
            }

        }while(number < 1 || number > 5);

        System.out.println(finalNumber);
    }
}
