package com.company;

public class Person {

    private String name;

    private String firstName;

    private String lastName;

    private int age;

    private String dni;

    private String adress;

    private String email;

    Person (String name, String firstName, String lastName, int age, String dni, String adress, String email) {

        this.name = name;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.dni = dni;
        this.adress = adress;
        this.email = email;
    }
}
