package com.company;

public class Validator {

    private final String REGEXP_DNI = "^(\\d{8}[TRWAGMYFPDXBNJZSQVHLCKE]$)";

    private final String REGEXP_EMAIL = "^[a-zA-Z0-9](\\.[a-zA-Z0-9]|[a-zA-Z0-9])*@([a-zA-Z0-9](\\.|-[a-zA-Z0-9]|[a-zA-Z0-9])*)$";

    private final String REGEXP_AGE = "^([0-9]|[1-9][0-9]|1[0-2][0-9])$";

    private final String REGEXP_NAME = "^.{0,45}$";

    private final String REGEXP_ADRESS = "^.{0,120}$";

    public boolean validateDNI(String dni) {

        return dni.matches(REGEXP_DNI);
    }

    public boolean validateEMAIL(String email) {

        return email.matches(REGEXP_EMAIL);
    }

    public boolean validateAGE(String age) {

        return age.matches(REGEXP_AGE);
    }

    public boolean validateNAME(String name) {

        return name.matches(REGEXP_NAME);
    }

    public boolean validateADRESS(String adress) {

        return adress.matches(REGEXP_ADRESS);
    }
}
