package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class PersonFactory {

    private Scanner sc;

    private Validator validator;

    private ArrayList<String> errorList;

    PersonFactory() {

        this.sc = new Scanner(System.in);
        this.validator = new Validator();
        this.errorList = new ArrayList<>();
    }

    public Person create() throws InvalidInputException{

        return new Person(getName(), getLastName(), getLastName(), getAge(), getDni(), getAdress(), getEmail());
    }

    private String getName() {

        System.out.println("Por favor introduce un nombre:");

        String name = this.sc.next();

        if (!validator.validateNAME(name)) {

            addErrors("Nombre invalido");

            throw new InvalidInputException("Nombre invalido");
        }

        return name;
    }

    private String getLastName() {

        System.out.println("Por favor introduce un apellido:");

        String lastName = this.sc.next();

        if (!validator.validateNAME(lastName)) {

            addErrors("Apellido invalido");

            throw new InvalidInputException("Apellido invalido");
        }

        return lastName;
    }

    private int getAge() {

        System.out.println("Por favor introduce una edad");

        String age = this.sc.next();

        if (!validator.validateAGE(age)) {

            addErrors("Edad invalida");

            throw new InvalidInputException("Edad invalida");
        }

        return Integer.parseInt(age);
    }

    private String getDni() {

        System.out.println("Por favor introduce un dni:");

        String dni = this.sc.next();

        if (!validator.validateDNI(dni)) {

            addErrors("Dni invalido");

            throw new InvalidInputException("Dni invalido");
        }

        return dni;
    }

    private String getAdress() {

        System.out.println("Por favor introduce una direccion:");

        String adress = this.sc.next();

        if (!validator.validateADRESS(adress)) {

            addErrors("Direccion invalida");

            throw new InvalidInputException("Direccion invalida");
        }

        return adress;
    }

    private String getEmail() {

        System.out.println("Por favor introduce un email:");

        String email = this.sc.next();

        if (!validator.validateEMAIL(email)) {

            addErrors("Email invalido");

            throw new InvalidInputException("Email invalido");
        }

        return email;
    }

    private void addErrors(String error) {

        this.errorList.add(error);
    }

    private boolean hasErrors() {

        return !this.errorList.isEmpty();
    }

    private String[] getErrors() {

        return this.errorList.toArray(new String[this.errorList.size()]);
    }
}
