package com.company;

public class Main {

    public static void main(String[] args) {

        PersonFactory personFactory = new PersonFactory();

        boolean okay = true;

        do {

            try {

                Person person = personFactory.create();

            } catch (InvalidInputException e) {

                okay = false;
            }

        } while (!okay);
    }
}
